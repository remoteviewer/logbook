# logbook
to help keep topical electronic logs

# to run

git clone https://github.com/bradleysimmons/logbook.git  
cd logbook  

command + t (mac) to open another terminal pane  

cd api  
npm install  
npm start  

(in other terminal pane)  
cd client  
npm install  
npm start  
 
open a browser to http://localhost:3000  

from the root url, you should be redirected to the set up screen, where you can create logs.  enter a title or identifier of your choice and click "create".  i like to think of these as topics of the logs, and the application is designed to encourage keeping multiple logs.  i usually title one log "general", where i write freely, and use other logs for more focused subjects.    
the selected log is indicated at the top of the screen.  click on a log title to select.  

in write mode, an entry is created automatically.  this entry will be available until you begin a new entry (by clicking "new").  

in list and read modes, you can view logs individually (by default), or compiled into one thread (by clicking "all").  when viewing all logs, click "by log" to return to the selected log.  

in list mode, click on an entry to view, edit, or delete.  

this is a prototype version.  please back up your logs if they are important to you: in the terminal pane running the api server, type s + enter to create a snapshot of the database.  




