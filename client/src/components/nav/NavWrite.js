import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { selectLog } from '../../actions';

class NavWrite extends React.Component {

  itemClassName() {
    return `button-padding  ${
      this.props.selectedMode === "write" ? "bordered" : ""
    }`;
  }

  actionButtonClassName() {
    return `button-padding cursor-pointer ${
      this.props.selectedMode !== "write" ? "display-none" : "bordered margin-x"
    }`;
  }

  componentDidUpdate() {
    if (this.props.firstVisibleLogId && !this.props.selectedLog) {
      this.props.selectLog(this.props.firstVisibleLogId)
    }
  }

  path() {

    const logId = this.props.selectedLog || this.props.firstVisibleLogId
    const entry = this.props.editingEntries[logId];

    if (logId && entry) {
      return `/logs/${logId}/entries/${entry["id"]}/edit`;
    } 
    if (logId && !entry){
      return `/logs/${logId}/entries/`;
    }
    if (!logId) {
      return "/";
    }
  }

  render() {
    return (
      <div className={`${this.itemClassName()} flex-row around`}>
        <Link
          to={this.path()}
          className="button-padding cursor-pointer"
          onClick={() => this.props.handleClick("write")}
        >
          write
        </Link>
        <span
          className={`${this.actionButtonClassName()}`}
          onClick={() => this.props.createEntry(this.props.selectedLog)}
        >
          new
        </span>
        <span
          ref={this.props.saveButtonRef}
          className={`${this.actionButtonClassName()}`}
        >
          save
        </span>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedLog: state.selectedLog,
    selectedMode: state.selectedMode,
    editingEntries: state.editingEntries,
    logs: Object.values(state.logs),
    firstVisibleLogId: state.firstVisibleLogId
  };
};

export default connect(
  mapStateToProps,
  {selectLog}
)(NavWrite);
