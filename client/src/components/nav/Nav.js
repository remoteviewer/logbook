import React from "react";
import { Link } from "react-router-dom";
import { connect } from 'react-redux';

import NavWrite from './NavWrite';
import NavList from './NavList';
import NavRead from './NavRead';

import { selectMode } from "../../actions";
import { selectLog } from '../../actions';
import { createEntry } from '../../actions';

class Nav extends React.Component {

  handleClick = (modeName) => {
    this.props.selectMode(modeName)
  }

  render() {
    return (
      <div
        className="flex-row-end around"
        style={{ position: "absolute", bottom: "13px", left: "0", right: "0" }}
      >
        <Link
          to="/logs/setup"
          className={`button-padding ${"setup" === this.props.selectedMode ? 'bordered' : ""}`}
          onClick={() => this.handleClick("setup")}
        >
          set up
        </Link>
        <NavList
          handleClick={this.handleClick}
        />
        <NavWrite  
          createEntry={this.props.createEntry}
          handleClick={this.handleClick}
          saveButtonRef={this.props.saveButtonRef}
        />
        <NavRead
          handleClick={this.handleClick}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedMode: state.selectedMode,
    selectedLog: state.selectedLog
  } 
}

export default connect(mapStateToProps, { selectMode, createEntry, selectLog })(Nav);
