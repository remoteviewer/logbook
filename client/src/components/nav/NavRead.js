import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class NavRead extends React.Component {

  itemClassName() {
    const mode = this.props.selectedMode.split(" ")[0]
    return `button-padding  ${mode === "read" ? 'bordered' : ""}`;
  }

  actionButtonClassName() {
    const mode = this.props.selectedMode.split(" ")[0]
    return `button-padding cursor-pointer ${mode !== "read" ? 'display-none' : "bordered margin-x"}`;
  }

  readOneModeHideButtonClassName() {
    const mode = this.props.selectedMode
    return `${mode === "read all one" || mode === "read one" ? "display-none" : ""}`;
  }

  readOneModeShowButtonClassName() {
    const mode = this.props.selectedMode
    return `${mode === "read all one" || mode === "read one" ? "" : 'display-none'}`;
  }

  actionButtonUrl() {
    const mode = this.props.selectedMode
    if (mode === "read all one" || mode === "read all") {
      return `/logs/${this.props.selectedLog}/entries/read`
    } else {
      return `/logs/index/entries/read`
    }
  }

  actionButtonHandleClickArg() {
    const mode = this.props.selectedMode
    console.log(mode)
    if (mode === "read all one" | mode === "read all") {
      return "read"
    } else {
      return "read all"
    }
  }

  actionButtonText() {
    const mode = this.props.selectedMode
    if (mode === "read all one" || mode === "read all") {
      return "by log"
    } else {
      return "all"
    }
  }

  render() {
    return (
      <div className={`${this.itemClassName()} flex-row around`}>
        <Link
          to={`/logs/${this.props.selectedLog}/entries/read`}
          className="button-padding cursor-pointer"
          onClick={() => this.props.handleClick("read")}
        >
          read
        </Link>
        <Link
          to={this.actionButtonUrl()} 
          className={`${this.actionButtonClassName()} ${this.readOneModeHideButtonClassName()}`}
          onClick={() => this.props.handleClick(this.actionButtonHandleClickArg())}
        >
          {this.actionButtonText()}
        </Link>
      </div>
    )
  }

}


const mapStateToProps = state => {
  return {
    selectedLog: state.selectedLog,
    selectedMode: state.selectedMode
  };
};

export default connect(mapStateToProps, {})(NavRead);



