import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { selectLog } from "../../actions";
import { getLogs } from "../../actions";

import { navHelper } from "../../helpers/navHelper.js";

class LogNav extends React.Component {

  state = {listHidden: false}

  componentDidMount() {
    this.props.getLogs();
  }

  componentWillReceiveProps({ firstVisibleLogId }) {
    if (this.props.selectedLog === null && firstVisibleLogId) {
      this.props.selectLog(firstVisibleLogId);
    }
  }

  isInDisabledMode() {
    const mode = this.props.selectedMode
    if (mode !== "setup" && mode !== "list all" && mode !== "read all") {
      return false;
    } else {
      return true;
    }
  }

  borderClass(log) {
    if (log.id === this.props.selectedLog) {
      if (this.isInDisabledMode()) {
        return "disabled-bordered"
      } else {
        return "bordered"
      }
    } else {
      return ""
    }
  }

  handleClick = logId => {
    if (this.isInDisabledMode() === false) {
      this.props.selectLog(logId);
    }
  };

  toggleListHidden = () => {
    this.setState({listHidden: !this.state.listHidden})
  }

  renderLogs() {
    return this.props.logs.map(log => {
      const isDisabled = this.isInDisabledMode()
      const disabledClass = isDisabled ? "disabled" : "";
      return (
        <Link
          to={navHelper(
            log.id,
            this.props.selectedMode,
            this.props.editingEntries
          )}
          className={`button-padding ${this.borderClass(log)} ${disabledClass}`}
          style={{ cursor: "pointer", whiteSpace: "nowrap" }}
          key={log.id}
          onClick={() => this.handleClick(log.id)}
        >
          {log.name}
        </Link>
      );
    });
  }

  selectedLogName = () => {
    const selectedLog = this.props.selectedLog
    const logs = this.props.logs
    const mode = this.props.selectedMode
    if (!selectedLog || logs.length === 0) {
      return "";
    } else {
      if (mode === "list all" || mode === "read all") {
        return "all"
      }
      if (mode === "setup") {
        return "set up"
      }
      return ""
    }
  }

  selectedLogDisplayNone() {
    if (this.isInDisabledMode()) {
      return ""
    } else {
      return "display-none"
    }
  }

  render() {
    if (!this.props.selectedLog) {
      return <div></div>
    }

    return (
      <div className="flex-row" style={{ position: "absolute", top: "5px", left: "0px", right: "0px"}}>
        <div className={`button-padding bordered cursor-pointer ${this.selectedLogDisplayNone()}`} style={{marginLeft: "5px"}} onClick={() => this.toggleListHidden()}>
          {this.selectedLogName()}
        </div>
        <div className={`${this.state.listHidden ? "display-none" : "flex-row around"}`}
          style={{flexGrow: "1"}}>
          {this.renderLogs()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedLog: state.selectedLog,
    selectedMode: state.selectedMode,
    editingEntries: state.editingEntries,
    firstVisibleLogId: state.firstVisibleLogId,
    logs: Object.values(state.logs)
  };
};

export default connect(
  mapStateToProps,
  { getLogs, selectLog }
)(LogNav);
