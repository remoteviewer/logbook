import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { createEntry, deleteEntry } from '../../actions';

import history from "../../history.js";

class NavList extends React.Component {

  itemClassName() {
    const mode = this.props.selectedMode.split(" ")[0]
    return `button-padding  ${mode === "list" ? 'bordered' : ""}`;
  }

  actionButtonClassName() {
    const mode = this.props.selectedMode.split(" ")[0]
    return `button-padding cursor-pointer ${mode !== "list" ? 'display-none' : "bordered margin-x"}`;
  }

  listOneModeHideButtonClassName() {
    const mode = this.props.selectedMode
    return `${mode === "list all one" || mode === "list one" ? "display-none" : ""}`;
  }

  listOneModeShowButtonClassName() {
    const mode = this.props.selectedMode
    return `${mode === "list all one" || mode === "list one" ? "" : 'display-none'}`;
  }

  actionButtonUrl() {
    const mode = this.props.selectedMode
    if (mode === "list all one" || mode === "list all") {
      return `/logs/${this.props.selectedLog}/entries/list`
    } else {
      return `/logs/index/entries/list`
    }
  }

  actionButtonHandleClickArg() {
    const mode = this.props.selectedMode
    console.log(mode)
    if (mode === "list all one" | mode === "list all") {
      return "list"
    } else {
      return "list all"
    }
  }

  actionButtonText() {
    const mode = this.props.selectedMode
    if (mode === "list all one" || mode === "list all") {
      return "by log"
    } else {
      return "all"
    }
  }

  editButtonUrl() {
    const entryId = this.props.entry ? this.props.entry.id : null
      return `/logs/${this.props.selectedLog}/entries/${entryId}/edit`
  }

  handleDeleteButtonClick() {
    if (this.props.entry) {
      this.props.deleteEntry(this.props.entry.id, this.props.selectedLog)
    }
  }

  render() {
    return (
      <div className={`${this.itemClassName()} flex-row around`}>
        <Link
          to={`/logs/${this.props.selectedLog}/entries/list`}
          className="button-padding cursor-pointer"
          onClick={() => this.props.handleClick("list")}
        >
          list
        </Link>
        <Link
          to={this.actionButtonUrl()} 
          className={`${this.actionButtonClassName()} ${this.listOneModeHideButtonClassName()}`}
          onClick={() => this.props.handleClick(this.actionButtonHandleClickArg())}
        >
          {this.actionButtonText()}
        </Link>
        <span
          className={`${this.actionButtonClassName()} ${this.listOneModeShowButtonClassName()}`}
          onClick={() => history.goBack()}
        >
          back
        </span>
        <Link
          to={this.editButtonUrl()}
          className={`${this.actionButtonClassName()} ${this.listOneModeShowButtonClassName()}`}
        >
          edit
        </Link>
        <span
          className={`${this.actionButtonClassName()} ${this.listOneModeShowButtonClassName()}`}
          onClick={() => { if (window.confirm('delete ?')) {this.handleDeleteButtonClick()}}}
        >
          delete
        </span>
      </div>
    )
  }

}


const mapStateToProps = state => {
  return {
    selectedLog: state.selectedLog,
    selectedMode: state.selectedMode,
    editingEntries: state.editingEntries,
    entry: state.entry,
    logs: Object.values(state.logs)
  };
};

export default connect(mapStateToProps, {createEntry, deleteEntry})(NavList);