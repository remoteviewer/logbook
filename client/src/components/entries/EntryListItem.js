import React from "react";
import { Link } from "react-router-dom";

class EntryListItem extends React.Component {
  
  state = {hover: false}

  handleHover = () => {
    this.setState({hover: !this.state.hover});
  }

  render() {
    const entry = this.props.entry
    const hoverClass = this.state.hover ? "bordered cursor-pointer button-padding" : ""
    return (
      <Link to={`/logs/${entry.logsId}/entries/${entry.id}/show`} style={{whiteSpace: "nowrap"}}>
        <div className={`${hoverClass}`} onMouseEnter={this.handleHover} onMouseLeave={this.handleHover}>
          <span className="date-span">{entry.createdAt}</span> {entry.text.slice(0, 77)}
        </div>
      </Link>
    )
  }
}

export default EntryListItem;