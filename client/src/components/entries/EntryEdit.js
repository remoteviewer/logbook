import React from 'react';
import { Field, reduxForm } from "redux-form";
import { connect } from 'react-redux';

import { editEntry, getEditingEntry, selectMode, clearEntry } from '../../actions'

class EntryEdit extends React.Component {

  constructor(props) {
    super(props)
    this.textareaRef = React.createRef();
  }

  componentDidMount() {
    this.props.selectMode("write")
    this.props.getEditingEntry(this.props.match.params.entries_id)
  }

  componentWillUnmount() {
    this.props.clearEntry()
  }

  componentDidUpdate() {
    if (this.textareaRef.current) {
       this.textareaRef.current.focus()
    }
    this.props.getEditingEntry(this.props.match.params.entries_id)
  }

  renderInput = ({ input }) => {
    return <textarea ref={this.textareaRef} className="full-width full-height" autoFocus {...input}></textarea>;
  }

  onBlur = (formValues) => {
    this.props.editEntry(this.props.entry.id, formValues, this.props.saveButtonRef.current)
  }

  render() {
    if (!this.props.entry) {
      return <div></div>;
    }
    return (
      <div className="full-width full-height">
        <form onBlur={this.props.handleSubmit(this.onBlur)} className="full-width full-height">
          <Field name="text" val={this.props.entry.text} component={this.renderInput} />
        </form>
      </div>
    )
  }
}

const formWrapped = reduxForm({form: "entryEdit", enableReinitialize : true})(EntryEdit);
const mapStateToProps = state => {
  return {
    entry: state.entry,
    initialValues: state.entry
  }; 
}

export default connect(mapStateToProps, { editEntry, getEditingEntry, selectMode, clearEntry })(formWrapped)