import React from 'react';
import { connect } from 'react-redux';
import { getLogsEntries, selectLog, selectMode } from '../../actions'

class EntryRead extends React.Component {
  
  componentDidMount() {
    if (this.props.match.params.id === "index") {
      this.props.selectMode("read all")
    } else {
      this.props.selectMode("read")
    }
    this.props.getLogsEntries(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (newProps.match.params.id !== this.props.match.params.id) {
      this.props.getLogsEntries(newProps.match.params.id)
    }
  }

  renderEntries() {
     return this.props.entries.map(entry => {
      if (entry.text.length === 0) {
        return null;
      }
      return (
        <div key={entry.id} style={{whiteSpace: 'pre-wrap'}}>
          <span className="date-span">{entry.createdAt}</span><br/>
          {entry.text}<br/><br/>
        </div>
      )
    });
  }

  render() {
    return(
      <div className="full-width full-height" style={{overflowY: "auto", overflowX: "hidden"}}>{this.renderEntries()}</div>
    )
  }
}

const mapStateToProps = (state) => {
  return {entries: Object.values(state.logsEntries)}
}

export default connect(mapStateToProps, { getLogsEntries, selectLog, selectMode })(EntryRead);