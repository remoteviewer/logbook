import React from 'react';
import { connect } from 'react-redux';

import { getEntry, selectLog, selectMode, clearEntry } from '../../actions'


class EntryShow extends React.Component {

  componentDidMount() {
    this.props.getEntry(this.props.match.params.id)
    this.props.selectLog(this.props.match.params.logs_id);
    const modeToSelect = this.props.selectedMode === "list all" ? "list all one" : "list one"
    this.props.selectMode(modeToSelect);
  }

  componentWillUnmount() {
    this.props.clearEntry()
  }

  render() {
    const entry = this.props.entry
    if (!entry) {
      return <div></div>
    }
    return (
      <div className="full-width full-height" style={{overflowY: "auto", overflowX: "hidden", whiteSpace: 'pre-wrap'}}>
       <span className="date-span">{entry.createdAt} - {entry.updatedAt}</span><br/><br/>
       {entry.text}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {entry: state.entry, selectedMode: state.selectedMode}
}

export default connect(mapStateToProps, {getEntry, selectLog, selectMode, clearEntry })(EntryShow);