import React from 'react';
import { connect } from 'react-redux';

import { createEntry } from '../../actions'

class EntryCreate extends React.Component {

  componentDidMount() {
    this.props.createEntry(this.props.selectedLog)
  }

  render() {
    return (
      <div></div>
    )
  }
}

const mapStateToProps = state => {
  return {selectedLog: state.selectedLog}
}

export default connect(mapStateToProps, {createEntry})(EntryCreate);