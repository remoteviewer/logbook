import React from 'react';
import { connect } from 'react-redux';
import { getLogsEntries, clearLogsEntries, selectMode, setScrollHeight } from '../../actions';

import EntryListItem from './EntryListItem'

class EntryList extends React.Component {

  constructor(props) {
    super(props)
    this.divRef = React.createRef();
    this.state = {scrollHeight: 0, mode: null}
  }

  componentDidMount() {
    if (this.props.match.params.id === "index") {
      this.props.selectMode("list all")
    } else {
      this.props.selectMode("list")
    }
    this.props.getLogsEntries(this.props.match.params.id)
    if (this.props.entryListScrollHeight) {
      this.divRef.current.scrollTop = this.props.entryListScrollHeight
      
    }
  }

  componentWillUnmount() {
    this.props.setScrollHeight(this.state.scrollHeight)
    this.props.clearLogsEntries()
  }

  componentWillReceiveProps(newProps) {
    if (newProps.match.params.id !== this.props.match.params.id) {
      this.props.getLogsEntries(newProps.match.params.id)
    }
  }

  renderList() {
    return this.props.entries.map(entry => {
      if (entry.text.length === 0) {
        return null;
      }
      return (
        <EntryListItem key={entry.id} entry={entry}/>
      )
    });
  }

  handleScroll = (e) => {
    this.setState({scrollHeight: e.target.scrollTop})
  }

  render() {
    return (
      <div className="full-width full-height" style={{overflowY: "auto", overflowX: "hidden"}} key={this.props.match.params.id} ref={this.divRef} onScroll={this.handleScroll}>
        {this.renderList()}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {entries: Object.values(state.logsEntries),
    entryListScrollHeight: state.entryListScrollHeight
  }
}

export default connect(mapStateToProps, {getLogsEntries, selectMode, setScrollHeight, clearLogsEntries})(EntryList);