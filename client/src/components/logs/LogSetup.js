import React from 'react';
import { connect } from 'react-redux';

import LogList from './LogList'
import LogCreate from './LogCreate'

import { selectMode, selectLog } from '../../actions'

class LogSetup extends React.Component {

  componentDidMount() {
    this.props.selectMode("setup")
  }

  render() {
    return (
      <div>
        <LogCreate />
        <LogList />
      </div>
    )
  }
}

export default connect(null, {selectMode, selectLog})(LogSetup);