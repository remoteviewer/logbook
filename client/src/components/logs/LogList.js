import React from 'react';
import { connect } from 'react-redux';

import { getLogs, deleteLog } from "../../actions";


class LogList extends React.Component {

  componentDidMount() {
    this.props.getLogs();
  }

  handleDeleteButtonClick = (logId) => {
    this.props.deleteLog(logId)
  }

  renderList() {
    return this.props.logs.map(log => {
      return (
        <div style={{width: "55vw", paddingBottom: "3px"}} className="flex-row between" key={log.id}>
          <span >
            {log.name}
          </span>
           <span
            className="button-padding bordered cursor-pointer"
            onClick={() => { if (window.confirm('delete ?  WARNING: this will also delete any entries for this log')) {this.handleDeleteButtonClick(log.id)}}}
            >
            delete
          </span>
        </div>
      )
    });
  }

  render() {
    return (
      <div style={{display: "flex", flexDirection: "column", marginTop: "25px", width: "55vw", marginLeft: "auto", marginRight: "auto"}}>
        {this.renderList()}
      </div>
    )
  } 
}

const mapStateToProps = state => {
  return {logs: Object.values(state.logs)} 
}

export default connect(mapStateToProps, { getLogs, deleteLog })(LogList);