import React from "react";
import { Field, Fields, reduxForm } from "redux-form";
import { connect } from 'react-redux';
import { createLog } from '../../actions'

class LogCreate extends React.Component {

  renderError({ name: { meta } }) {
    if (meta.submitFailed && meta.error) {
      return (
        <div>{meta.error}</div>
      )
    }
    return null;
  } 
  
  renderInput = ({ input, label }) => {
    return (
      <div>
        <label>{label}</label>
        <input style={{width: "30vw"}} className="text-input button-padding" autoFocus autoComplete="off" {...input} />
      </div>
    )
  }

  onSubmit = (formValues) => {
    this.props.createLog(formValues)
  }

  render() {
    return (
      <div style={{marginTop: '10px'}}>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)} className="flex-row center">
          <Field name="name" label="new log name: " component={this.renderInput} />
          <button className="button-padding bordered">create</button>
        </form>
        <Fields names={["name"]} component={this.renderError}/>
      </div>
    );
  }
}

const validate = (formValues) => {

  const errors = {}

  if (!formValues.name) {
    errors.name = 'new log must have a name';
  }

  return errors;
}

const formWrapped = reduxForm({
  form: "logCreate",
  validate
})(LogCreate);

export default connect(null, { createLog })(formWrapped)
