import React from 'react';
import { connect } from 'react-redux';
import history from "../history.js";

class Landing extends React.Component {

  componentDidUpdate() {
    if (this.props.selectedLog) {
      history.push(`/logs/${this.props.selectedLog}/entries/`)
    }
  }

  render() {
    return (
      <div>
       loading...
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {selectedLog: state.selectedLog}
}

export default connect(mapStateToProps, {})(Landing);