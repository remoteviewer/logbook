import React from 'react';
import  { Router, Route, Switch } from 'react-router-dom';

import '../css/shared.css'

import EntryCreate from './entries/EntryCreate';
import EntryDelete from './entries/EntryDelete';
import EntryEdit from './entries/EntryEdit';
import EntryList from './entries/EntryList';
import EntryRead from './entries/EntryRead';
import EntryShow from './entries/EntryShow';
import LogSetup from './logs/LogSetup';
import LogDelete from './logs/LogDelete';
import LogEdit from './logs/LogEdit';
import LogShow from './logs/LogShow';
import Nav from './nav/Nav';
import LogNav from './nav/LogNav';
import Landing from './Landing';

import history from '../history.js';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.saveButtonRef = React.createRef();
  }

  render() {
    return (
      <div style={{height: '100%', width: '65vw'}}>
        <Router history={history}>
          <LogNav />
          <div style={{position: "absolute", top: "65px", bottom: "78px", width: '65vw'}}>
            <Switch>
              <Route path="/" exact component={Landing} />
              <Route path="/logs/:id/entries/list" exact component={EntryList} />
              <Route path="/logs/:id/entries/read" exact component={EntryRead} />
              <Route path="/entries/:id/delete" exact component={EntryDelete} />
              <Route path="/logs/:logs_id/entries/" exact component={EntryCreate} />
              <Route path="/logs/:logs_id/entries/:entries_id/edit" exact render={(props) => <EntryEdit {...props} saveButtonRef={this.saveButtonRef} />}/>
              <Route path="/logs/:logs_id/entries/:id/show" exact component={EntryShow} />
              <Route path="/logs/setup" exact component={LogSetup} />
              <Route path="/logs/:id/delete" exact component={LogDelete} />
              <Route path="/logs/:id/edit" exact component={LogEdit} />
              <Route path="/logs/:id/show" exact component={LogShow} />
            </Switch>
          </div>
          <Nav saveButtonRef={this.saveButtonRef}/>
        </Router>
      </div>
    )
  }
}
export default App;