import jsonServer from '../apis/jsonServer';
import history from "../history";

import { flashSaveButton } from "../helpers/flashSaveButton";

import { 
  SELECT_LOG,
  SELECT_MODE,
  CREATE_LOG,
  DELETE_LOG,
  EDIT_LOG,
  GET_LOGS,
  GET_LOG,
  CREATE_ENTRY,
  GET_EDITING_ENTRY,
  EDIT_ENTRY,
  GET_LOGS_ENTRIES,
  CLEAR_LOGS_ENTRIES,
  GET_ENTRY,
  CLEAR_ENTRY,
  DELETE_ENTRY,
  SET_SCROLL_HEIGHT
} from './types'

export const selectLog = logsId  => {
  return {
    type: SELECT_LOG,
    payload: logsId
  }
}

export const selectMode = modeName => {
  return {
    type: SELECT_MODE,
    payload: modeName
  }
}

export const setScrollHeight = (scrollHeight) => {
  return {
    type: SET_SCROLL_HEIGHT,
    payload: scrollHeight
  }
}

export const createLog = formValues => async dispatch => {
  const response = await jsonServer.post('/logs', formValues);
  dispatch({
    type: CREATE_LOG,
    payload: response.data
  })
};

export const deleteLog = id => async dispatch => {
  await jsonServer.delete(`/logs/${id}`);
  dispatch({
    type: DELETE_LOG,
    payload: id
  })
};

export const editLog = (id, formValues) => async dispatch => {
  const response = await jsonServer.patch(`/logs/${id}`, formValues);
  dispatch({
    type: EDIT_LOG,
    payload: response.data
  })
};

export const getLogs = () => async dispatch => {
  const response = await jsonServer.get('/logs');
  dispatch({
    type: GET_LOGS,
    payload: response.data
  })
  if (response.data.length === 0) {
    history.push(`/logs/setup`)
  }
  
};

export const getLog = id => async dispatch => {
  const response = await jsonServer.get(`/logs/${id}`);
  dispatch({
    type: GET_LOG,
    payload: response.data
  })
};

export const createEntry = (logsId) => async dispatch => {
  const createdAt = new Date(Date.now()).toLocaleString();
  const response = await jsonServer.post('/entries', {logsId: logsId, text: "", createdAt: createdAt});
  dispatch({
    type: CREATE_ENTRY,
    payload: response.data
  })
  history.push(`/logs/${logsId}/entries/${response.data['id']}/edit`)
};


export const getEditingEntry = id => async dispatch => {
  const response = await jsonServer.get(`/entries/${id}`)
  dispatch({
    type: GET_EDITING_ENTRY,
    payload: response.data
  })
};

export const editEntry = (id, formValues, saveButtonRef) => async dispatch => {
  const updatedAt = new Date(Date.now()).toLocaleString();
  const response = await jsonServer.patch(`/entries/${id}`, {...formValues, updatedAt: updatedAt});
  flashSaveButton(saveButtonRef)
  dispatch({
    type: EDIT_ENTRY,
    payload: response.data
  })
};

export const getLogsEntries = logsId => async dispatch => {
  const url = logsId === "index" ? "/entries" : `/entries?logsId=${logsId}`
  const response = await jsonServer.get(url);
  dispatch({
    type: GET_LOGS_ENTRIES,
    payload: response.data
  })
};

export const clearLogsEntries = () => {
  return {
    type: CLEAR_LOGS_ENTRIES
  }
};

export const getEntry = id => async dispatch => {
  const response = await jsonServer.get(`/entries/${id}`);
  dispatch({
    type: GET_ENTRY,
    payload: response.data
  })
};

export const clearEntry = () => {
  return {
    type: CLEAR_ENTRY
  }
};

export const deleteEntry = (id, selectedLog) => async dispatch => {
  await jsonServer.delete(`/entries/${id}`);
  dispatch({
    type: DELETE_ENTRY,
    payload: selectedLog
  })
  history.push(`/logs/${selectedLog}/entries/list`)
};