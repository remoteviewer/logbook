// export const INITIALIZE_SELECTED_LOG = 'INITIALIZE_SELECTED_LOG';
export const SELECT_LOG = 'SELECT_LOG';
export const SELECT_MODE = 'SELECT_MODE';
export const SET_SCROLL_HEIGHT = 'SET_SCROLL_HEIGHT';

export const CREATE_LOG = 'CREATE_LOG';
export const DELETE_LOG = 'DELETE_LOG';
export const EDIT_LOG = 'EDIT_LOG';
export const GET_LOGS = 'GET_LOGS';
export const GET_LOG = 'GET_LOG';

export const CREATE_ENTRY = 'CREATE_ENTRY';
export const GET_EDITING_ENTRY = 'GET_ENTRY';
export const EDIT_ENTRY = 'EDIT_ENTRY';
export const GET_LOGS_ENTRIES = 'GET_LOGS_ENTRIES';
export const CLEAR_LOGS_ENTRIES = 'CLEAR_LOGS_ENTRIES';
export const GET_ENTRY = 'GET_ENTRY';
export const CLEAR_ENTRY = 'CLEAR_ENTRY';
export const DELETE_ENTRY = 'DELETE_ENTRY';

  