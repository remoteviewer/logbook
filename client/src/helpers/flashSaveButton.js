export function flashSaveButton(saveButtonRef) {
  saveButtonRef.classList.add("background-green")
  setTimeout(function() { 
    saveButtonRef.classList.remove("background-green")
  }, 500)
}