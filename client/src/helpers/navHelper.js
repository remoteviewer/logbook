export function navHelper(selectedLogId, selectedMode, editingEntries) {
  switch (selectedMode) {
    case "write":
      const entry = editingEntries[selectedLogId]
      if (entry) {
        return `/logs/${selectedLogId}/entries/${entry['id']}/edit`
      } else {
        return `/logs/${selectedLogId}/entries/`
      }
    case "list":
      return `/logs/${selectedLogId}/entries/list`
    case "list all":
      return "/logs/index/entries/list"
    case "read":
      return `/logs/${selectedLogId}/entries/read`
    case "read all":
      return "/logs/index/entries/read"
    case "setup":
      return "/logs/setup"
    default:
      return "/"
  }
};

