import { SELECT_MODE } from '../actions/types'

const INITIAL_STATE = 'write';

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SELECT_MODE:
      return action.payload;
    default:
      return state;
  }
};