import { SET_SCROLL_HEIGHT } from '../actions/types'

export default (state = 0, action) => {
  switch (action.type) {
    case SET_SCROLL_HEIGHT:
      return action.payload;
    default:
      return state;
  }
};