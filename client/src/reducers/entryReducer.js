import { CREATE_ENTRY, EDIT_ENTRY, GET_EDITING_ENTRY, GET_ENTRY, DELETE_ENTRY, CLEAR_ENTRY } from '../actions/types'

export default (state = null, action) => {
  switch (action.type) {
    case GET_EDITING_ENTRY:
      return action.payload
    case CREATE_ENTRY:
      return action.payload
    case EDIT_ENTRY:
      return action.payload
    case GET_ENTRY:
    console.log(action.payload)
      return action.payload
    case DELETE_ENTRY:
      return null
    case CLEAR_ENTRY:
      return null
    default:
      return state;
  }
};