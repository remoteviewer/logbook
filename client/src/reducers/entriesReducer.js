import _ from 'lodash';
import { GET_LOGS_ENTRIES, CLEAR_LOGS_ENTRIES } from '../actions/types'

export default (state = {}, action) => {
  switch (action.type) {
    case GET_LOGS_ENTRIES:
      return {..._.mapKeys(action.payload, 'id')};
    case CLEAR_LOGS_ENTRIES:
      return {};
    default:
      return state;
  }
};