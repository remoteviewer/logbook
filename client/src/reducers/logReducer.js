import _ from 'lodash';

import { 
  CREATE_LOG,
  DELETE_LOG,
  EDIT_LOG,
  GET_LOGS,
  GET_LOG  
} from '../actions/types'

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_LOG:
      return { ...state, [action.payload.id]: action.payload};
    case DELETE_LOG:
      return _.omit(state, action.payload);
    case EDIT_LOG:
      return { ...state, [action.payload.id]: action.payload};
    case GET_LOGS:
      return { ...state, ..._.mapKeys(action.payload, 'id')};
    case GET_LOG:
      return { ...state, [action.payload.id]: action.payload};
    default:
      return state;
  }
};

    