import _ from 'lodash';

import { CREATE_ENTRY, EDIT_ENTRY, DELETE_ENTRY } from '../actions/types'

export default (state = {}, action) => {
  switch (action.type) {
    case CREATE_ENTRY:
      return { ...state, [action.payload.logsId]: action.payload};
    case EDIT_ENTRY:
      return { ...state, [action.payload.logsId]: action.payload};
    case DELETE_ENTRY:
      return _.omit(state, action.payload);
    default:
      return state;
  }
};