import  { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import selectedLogReducer from './selectedLogReducer';
import selectedModeReducer from './selectedModeReducer';
import entryListScrollHeightReducer from './entryListScrollHeightReducer';
import logReducer from './logReducer';
import entryReducer from './entryReducer';
import entriesReducer from './entriesReducer';
import editingEntriesReducer from './editingEntriesReducer';
import firstVisibleLogIdReducer from './firstVisibleLogIdReducer';

export default combineReducers({
  logs: logReducer,
  entry: entryReducer,
  firstVisibleLogId: firstVisibleLogIdReducer,
  logsEntries: entriesReducer,
  selectedLog: selectedLogReducer,
  selectedMode: selectedModeReducer,
  entryListScrollHeight: entryListScrollHeightReducer,
  editingEntries: editingEntriesReducer,
  form: formReducer
});