import { SELECT_LOG } from '../actions/types'

export default (state = null, action) => {
  switch (action.type) {
    case SELECT_LOG:
      return parseInt(action.payload);
    default:
      return state;
  }
};
