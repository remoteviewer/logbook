import { GET_LOGS } from '../actions/types'

export default (state = null, action) => {
  switch (action.type) {
    case GET_LOGS:
      if (action.payload.length > 0) {
        return action.payload[0]['id']
      }
      return null;
    default:
      return state;
  }
};

    
